﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace R
{
    class Program
    {
        static void Main(string[] args)
        {
            MyHttpServer myhttpserver = new MyHttpServer();
            HttpRequester httprequester = new HttpRequester();
            Category cat = new Category(1, "balleck", true);
            Product prod = new Product(2, "onestla", 6);

            cat.getA();
            prod.getB();

            myhttpserver.ServerInitialisation();
            httprequester.Httpget();
            httprequester.Httpsent(cat.getA() + prod.getB());
            Console.ReadKey();
        }
    }
}
