﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace R
{
    public class SqlCo
    {
        public void SelectQuery(string query)
        {
            String connectionString = "SERVER=127.0.0.1;DATABASE=crouspeed;UID=root;PASSWORD=";
            MySqlConnection oConnection = new MySqlConnection(connectionString);

            MySqlCommand cmd = oConnection.CreateCommand();
            cmd.CommandText = query;

            try
            {
                oConnection.Open();
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }

            MySqlDataReader readcmd;

            readcmd = cmd.ExecuteReader();
            readcmd.Close();

            oConnection.Close();

        }
    }
}
