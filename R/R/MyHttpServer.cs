﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace R
{
    class MyHttpServer
    {

        public void ServerInitialisation()
        {
            Product prod = new Product(1, "ballec", 6);

            HttpListener listener = null;
            try
            {
                listener = new HttpListener();
                listener.Prefixes.Add("http://localhost:1300/simpleserver/");
                listener.Start();

                while (true)
                {
                    Console.WriteLine("waiting..");
                    HttpListenerContext context = listener.GetContext();
                    string msg = "hello";
                    context.Response.ContentLength64 = Encoding.UTF8.GetByteCount(msg + prod.getB());
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    using (Stream stream = context.Response.OutputStream)
                    {
                        using (StreamWriter write = new StreamWriter(stream))
                        {
                            write.Write(msg + prod.getB());
                        }

                    }
                    Console.WriteLine("msg sent..");

                }

            }
            catch(WebException e)
            {
                Console.WriteLine(e.Status);
            }
                

            }
        }
        
    
}
